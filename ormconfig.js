// eslint-disable-next-line @typescript-eslint/no-var-requires
const { ConfigUtils } = require('./src/server/core/config/config.utils');

// TypeORM CLI expects a promise to be returned directly from the config file.
module.exports = (async () => {
    const appConfig = await ConfigUtils.getStandaloneConfigServiceNestApp();

    return {
        type: 'postgres',
        ...appConfig.db,
        entities: ['src/**/*.entity.ts'],
        migrations: ['src/server/migrations/[1234567890]*.ts'],
        synchronize: false,
        cli: {
            migrationsDir: 'src/server/migrations',
        },
    };
})();
