to start the project:

- node version 16.13.0
- run "npm run start:dev" from the root of the project
- open the app in your browser on "localhost:3000"

working with the database:

INSTALL Postgresql and Pgadmin 4

- install postgresql, version 13.1, download from here https://www.postgresql.org/download/windows/
- install https://www.pgadmin.org/ for working with postgesql database
  - open pgadmin, click on servers and then on PostgreSQL 13 (default), enter the paswword 'postgres' after which you will get access to the default server
  - you should now see two sections: 1. Databases, 2. Login/Group Roles
  - click with the right click on the Login/Group Roles and then Create
  - in the name and password fields enter 'cms_local' and then in privileges set everything to 'yes'
- now we need to create new server; click with the right click on Servers then Create/Server:
  - in name field give it some name of your choice like 'cms_local_db' or whatever
  - in connection you need to enter: host, port, maintance db, username and password
  - you can read these from .env.a file
  - click save

CREATE Schema and run migrations

to create/drop database just run "npm run db:reset:a", this will always work

however you maybe want not to lose data but anyway just run migrations or seeds in that case you can run "npm run db:migrate" or "npm run db:seed"

