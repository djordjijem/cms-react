import React from 'react';
import HomePage from '../features/home/HomePage';
import { NextPage } from 'next';

const Home: NextPage = () => {
    return <HomePage />;
};

export default Home;
