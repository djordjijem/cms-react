import Layout from '../components/layout/Layout';
import '../styles/globalStyles.scss';
import Head from 'next/head';
import { store } from '../redux/store';
import { Provider } from 'react-redux';

const MyApp = ({ Component, pageProps }) => {
    return (
        <>
            <Head>
                <meta
                    name='viewport'
                    content='initial-scale=1, width=device-width'
                />
            </Head>
            <Provider store={store}>
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </Provider>
        </>
    );
};

export default MyApp;
