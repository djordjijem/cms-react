import Joi from 'joi';

const string = Joi.string();

export const newTagSchema = Joi.object({
    newTag: string.trim().required().messages({
        'string.empty': 'No empty tags!',
    }),
});

export const newHeadingSchema = Joi.object({
    headingInput: string.trim().required().messages({
        'string.empty': 'Heading is mandatory!',
    }),
    headingText: string.trim().allow('').optional(),
});

export const newCommentSchema = Joi.object({
    newComment: string.trim().required().messages({
        'string.empty': 'No empty comments!',
    }),
});

export const newReplySchema = Joi.object({
    newReply: string.trim().required().messages({
        'string.empty': 'No empty replies!',
    }),
});
