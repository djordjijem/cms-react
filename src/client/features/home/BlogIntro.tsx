import React, { useState } from 'react';

import { Typography, Box, TextField } from '@mui/material';

import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';

const BlogIntro: React.FC = () => {
    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const [blogIntroText, setBlogIntroText] = useState('Blog Intro Text');

    const handleBlogIntroChange = (e: React.ChangeEvent<HTMLInputElement>) =>
        setBlogIntroText(e.target.value);

    return (
        <Box mt={!isAdminActive && !blogIntroText ? 0 : 2}>
            {isAdminActive && (
                <TextField
                    value={blogIntroText}
                    onChange={handleBlogIntroChange}
                    fullWidth
                    multiline
                    rows={4}
                    label='Please enter blog intro text...'
                    variant='outlined'
                    color='primary'
                    InputProps={{
                        style: {
                            fontSize: 16,
                        },
                    }}
                    InputLabelProps={{
                        style: { fontSize: 16 },
                    }}
                />
            )}

            {!isAdminActive && (
                <Box pl={2} borderLeft={5} borderColor='primary.light'>
                    <Typography
                        color='grey.800'
                        fontSize='2.2rem'
                        fontStyle='italic'
                    >
                        {blogIntroText}
                    </Typography>
                </Box>
            )}
        </Box>
    );
};

export default BlogIntro;
