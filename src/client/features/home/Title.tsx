import React from 'react';

import { disableSubmitOnEnterKey } from '../../utils/utils';

import styles from '../../styles/components/title.module.scss';
import { Typography, Box, TextField } from '@mui/material';
import Grow from '@mui/material/Grow';

import { RootState } from '../../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { titleSlice } from '../../redux/slices/titleSlice';

const Title: React.FC = () => {
    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const title = useSelector((state: RootState) => state.title.title);

    const dispatch = useDispatch();

    const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) =>
        dispatch(titleSlice.actions.updateTitle(e.target.value));

    return (
        <Box display='flex' alignItems='center' minHeight={80} mb={1}>
            {isAdminActive && (
                <Grow in timeout={1000}>
                    <form
                        className={styles['title__form']}
                        noValidate
                        autoComplete='off'
                    >
                        <TextField
                            name='title'
                            value={title}
                            onChange={handleTitleChange}
                            autoFocus
                            label='Please enter title text...'
                            variant='outlined'
                            color='primary'
                            fullWidth
                            InputProps={{
                                style: {
                                    fontSize: 18,
                                },
                            }}
                            InputLabelProps={{
                                style: { fontSize: 18 },
                            }}
                            error={title === ''}
                            onKeyPress={disableSubmitOnEnterKey}
                        />
                    </form>
                </Grow>
            )}

            {!isAdminActive && (
                <Grow in timeout={1000}>
                    <Typography variant='h1'>{title}</Typography>
                </Grow>
            )}
        </Box>
    );
};

export default Title;
