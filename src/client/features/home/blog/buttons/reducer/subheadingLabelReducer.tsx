export enum SubheadingActionType {
    ADD_NEW = 'ADD_NEW_SUBHEADING_LABEL',
    ADD_FIRST = 'ADD_FIRST_SUBHEADING_LABEL',
}

enum SubheadingLabel {
    FIRST_LABEL = 'Subheading1',
}

export const subheadingLabelInitialState: string[] = [];

export const subheadingLabelReducer = (
    state: string[],
    action: { type: string; value?: string },
) => {
    const { type, value } = action;

    switch (type) {
        case SubheadingActionType.ADD_NEW:
            return [...state, value];
        case SubheadingActionType.ADD_FIRST:
            return [SubheadingLabel.FIRST_LABEL];
        default:
            return state;
    }
};
