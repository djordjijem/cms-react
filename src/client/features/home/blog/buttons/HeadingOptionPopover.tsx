import React, { SetStateAction, useReducer } from 'react';

import HeadingInputField from '../inputs/HeadingInputField';
import HeadingOptionBtn from './HeadingOptionBtn';

import styles from '../../../../styles/components/blog.module.scss';
import { Popover, Box } from '@mui/material';

import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import { newHeadingSchema } from '../../../../joi-schemas/joiSchemas';

import { BlogPostType } from '../../../../types/types';

import { v4 as uuidv4 } from 'uuid';

import { BtnType } from './HeadingOptionBtn';
import { InputType } from '../inputs/HeadingInputField';

import {
    subheadingLabelReducer,
    subheadingLabelInitialState,
} from './reducer/subheadingLabelReducer';

interface HeadingPopoverProps {
    id: string;
    isOpen: boolean;
    anchorEl: any;
    handleCloseClick: () => void;
    setBlogPost?: React.Dispatch<SetStateAction<BlogPostType[]>>;
}

const HeadingOptionPopover: React.FC<HeadingPopoverProps> = ({
    id,
    isOpen,
    anchorEl,
    handleCloseClick,
    setBlogPost,
}) => {
    const [subheadingLabelArray, dispatch] = useReducer(
        subheadingLabelReducer,
        subheadingLabelInitialState,
    );

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm({
        resolver: joiResolver(newHeadingSchema),
    });

    const addNewHeading = (data: {
        headingInput: string;
        headingText: string;
    }) => {
        const id = uuidv4();
        const { headingInput, headingText } = data;

        const newHeading = { id, heading: headingInput, headingText };

        setBlogPost((prevState) => [...prevState, newHeading]);

        reset();

        handleCloseClick();
    };

    return (
        <Popover
            id={id}
            open={isOpen}
            anchorEl={anchorEl}
            onClose={handleCloseClick}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
        >
            <Box maxWidth='900px' p={2}>
                <form
                    className={styles['blog__new-heading-form']}
                    onSubmit={handleSubmit(addNewHeading)}
                    noValidate
                    autoComplete='off'
                >
                    <HeadingInputField
                        inputType={InputType.HEADING_INPUT}
                        register={register}
                        errors={errors}
                    />

                    <HeadingInputField
                        inputType={InputType.HEADING_TEXT}
                        register={register}
                        errors={errors}
                    />

                    {subheadingLabelArray?.map((subheading) => (
                        <HeadingInputField
                            key={subheading}
                            inputType={InputType.SUBHEADING}
                            register={register}
                            errors={errors}
                        />
                    ))}

                    <Box mt={1} display='flex' justifyContent='space-between'>
                        <Box>
                            <HeadingOptionBtn
                                btnType={BtnType.SUB}
                                dispatch={dispatch}
                                subheadingLabelArray={subheadingLabelArray}
                            />

                            <HeadingOptionBtn btnType={BtnType.IMG} />
                        </Box>
                        <Box>
                            <HeadingOptionBtn btnType={BtnType.SAVE} />

                            <HeadingOptionBtn btnType={BtnType.CLOSE} />
                        </Box>
                    </Box>
                </form>
            </Box>
        </Popover>
    );
};

export default HeadingOptionPopover;
