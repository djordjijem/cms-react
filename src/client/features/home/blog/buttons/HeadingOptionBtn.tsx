import React, { useState, useMemo, SetStateAction } from 'react';

import CustomButton from '../../../../components/button/CustomButton';
import HeadingOptionPopover from './HeadingOptionPopover';

import styles from '../../../../styles/components/blog.module.scss';
import {
    AiOutlineClose,
    AiOutlineCheck,
    AiOutlineMinus,
    AiOutlinePlus,
} from 'react-icons/ai';

import { BlogPostType } from '../../../../types/types';

import { SubheadingActionType } from './reducer/subheadingLabelReducer';

export enum BtnType {
    SAVE = 'save',
    CLOSE = 'close',
    ADD = 'add',
    SUB = 'sub',
    IMG = 'img',
}

interface HeadingOptionBtnProps {
    btnType:
        | BtnType.SAVE
        | BtnType.CLOSE
        | BtnType.ADD
        | BtnType.SUB
        | BtnType.IMG;
    setBlogPost?: React.Dispatch<SetStateAction<BlogPostType[]>>;
    dispatch?: React.Dispatch<{ type: string; value?: string }>;
    subheadingLabelArray?: string[];
}

enum AddNewHeading {
    simplePopover = 'simple-popover',
    addHeadingBtnText = 'Add New Heading',
    addSubheadingBtnText = 'Subheading',
    addImgBtnText = 'Image',
}

const HeadingOptionBtn: React.FC<HeadingOptionBtnProps> = ({
    btnType,
    setBlogPost,
    dispatch,
    subheadingLabelArray,
}) => {
    const [anchorEl, setAnchorEl] = useState<EventTarget | null>(null);
    const isOpen = Boolean(anchorEl);
    const id = isOpen ? AddNewHeading.simplePopover : undefined;

    const handleAddHeadingClick = (e: { currentTarget: EventTarget }) =>
        setAnchorEl(e.currentTarget);

    const handleCloseClick = () => setAnchorEl(null);

    const handleAddSubheadingClick = () => {
        if (!subheadingLabelArray.length) {
            dispatch({
                type: SubheadingActionType.ADD_FIRST,
            });

            return;
        }

        const lastSubheadingLabel =
            subheadingLabelArray[subheadingLabelArray.length - 1];

        const lastLabelNumber = parseInt(lastSubheadingLabel.slice(10)) + 1;

        const newSubheadingLabel = `Subheading${lastLabelNumber}`;

        dispatch({
            type: SubheadingActionType.ADD_NEW,
            value: newSubheadingLabel,
        });
    };

    const setButtonFunctionToApply = useMemo(() => {
        return btnType === BtnType.ADD
            ? handleAddHeadingClick
            : btnType === BtnType.CLOSE
            ? handleCloseClick
            : btnType === BtnType.SUB
            ? handleAddSubheadingClick
            : null;
    }, [btnType]);

    const setButtonStartIcon = useMemo(() => {
        const saveBtnIcon = <AiOutlineCheck size='1rem' />;
        const closeBtnIcon = <AiOutlineClose size='1rem' />;
        const addHeadingBtnIcon = isOpen ? (
            <AiOutlineMinus />
        ) : (
            <AiOutlinePlus />
        );
        const addSubheadingAndImageBtnIcon = <AiOutlinePlus size='1rem' />;

        return btnType === BtnType.SAVE
            ? saveBtnIcon
            : btnType === BtnType.CLOSE
            ? closeBtnIcon
            : btnType === BtnType.ADD
            ? addHeadingBtnIcon
            : addSubheadingAndImageBtnIcon;
    }, [btnType]);

    const setButtonClassToApply = useMemo(() => {
        return btnType === BtnType.SAVE
            ? styles['blog__new-heading-margin-btn']
            : btnType === BtnType.SUB
            ? styles['blog__new-heading-margin-btn']
            : null;
    }, [btnType]);

    const setButtonTextToApply = useMemo(() => {
        return btnType === BtnType.ADD
            ? AddNewHeading.addHeadingBtnText
            : btnType === BtnType.SUB
            ? AddNewHeading.addSubheadingBtnText
            : btnType === BtnType.IMG
            ? AddNewHeading.addImgBtnText
            : btnType;
    }, [btnType]);

    return (
        <>
            <CustomButton
                className={setButtonClassToApply}
                startIcon={setButtonStartIcon}
                onClick={setButtonFunctionToApply}
                aria-described-by={btnType === BtnType.ADD && id}
                color='primary'
                variant='outlined'
                text={setButtonTextToApply}
                type={btnType === BtnType.SAVE ? 'submit' : 'button'}
            />

            {btnType === BtnType.ADD && (
                <HeadingOptionPopover
                    id={id}
                    isOpen={isOpen}
                    anchorEl={anchorEl}
                    handleCloseClick={handleCloseClick}
                    setBlogPost={setBlogPost}
                />
            )}
        </>
    );
};

export default HeadingOptionBtn;
