import React, { useState } from 'react';

import HeadingOptionBtn from './buttons/HeadingOptionBtn';

import Image from 'next/image';

import styles from '../../../styles/components/blog.module.scss';
import { Box, Container, Typography } from '@mui/material';

import { useSelector } from 'react-redux';
import { RootState } from '../../../redux/store';

import { BlogPostType } from '../../../types/types';

const blogPost2 = [
    {
        id: 1,
        heading: '1. Application structure',
        text: 'You should have Go installed and ready by now. Open up your favorite IDE for Go (Visual Studio Code, GoLand, ...) and create a new Go project. As I mentioned earlier, the idea is to build a simple REST API for book store management by using Mux. Once you created your blank project, create the following structure inside of it:',
        imgSrc: '/img/home-img.jpg',
        subtopics: [
            {
                id: 1,
                heading: '1.1 Go packages and modules',
                text: "This is the right time to talk about Go modules and packages. If you are familiar with Python, you might get an idea of what those are since they operate similarly. The best way to describe a Go package is that it's a collection of source files in the same directory that are compiled together as a reusable unit. That means that all files that serve a similar purpose should be put inside one package. As per our structure above - src is one of our packages. Go module is a collection of Go packages along with their dependencies, meaning that one module can consist of multiple packages. You can think of our whole application as a Go module for easier understanding.",
            },
        ],
    },
    {
        id: 2,
        heading: '2. Building the API',
        text: 'It is time to start building our application. Open up your main.go file and insert the following code inside of it:',
        imgSrc: '/img/home-img.jpg',
        subtopics: [
            {
                id: 1,
                heading: '2.1 Routes and handlers',
                text: 'Now we need to create our API router (Mux) and configure it by creating some endpoints and their handlers. Inside of your src package open up app.go and insert the following code inside of it:',
                imgSrc: '/img/home-img.jpg',
            },
            {
                id: 2,
                heading: '2.2 Middlewares',
                text: 'As we all know - REST APIs mostly use JSON when taking requests and returning responses. That is communicated to our browsers/HTTP clients by making use of Content-Type headers. Since our API will only be using JSON-represented data, we can make use of a middleware that will make sure our content type is always set to JSON.',
                imgSrc: '/img/home-img.jpg',
            },
            {
                id: 3,
                heading: '2.3 Static data',
                text: "Since we won't be using any data storage services (database, cache, ...) we need to have some sort of static data. Also, we will create a data type for custom responses, which I will explain later on.",
                imgSrc: '/img/home-img.jpg',
            },
        ],
    },
    {
        id: 3,
        heading: '3. Running and testing the API',
        text: 'You should have Go installed and ready by now. Open up your favorite IDE for Go (Visual Studio Code, GoLand, ...) and create a new Go project. As I mentioned earlier, the idea is to build a simple REST API for book store management by using Mux. Once you created your blank project, create the following structure inside of it:',
        subtopics: [
            {
                id: 1,
                heading: '3.1 Delete existing book',
                text: 'After we get the integer value of the book_id variable, we iterate over the booksDB to find the book that the user wants to delete. If it exists, we make use of our helper removeBook function to remove the book from the Book struct slice. If it does not exist, we will return the 404: Not Found error back.',
                subtopics: [
                    {
                        id: 1,
                        heading: '3.1.1 Add a new book',
                        text: 'Once we decode and validate the JSON body against our Book struct (if it fails we will return the 400: Bad Request error), we need to check if the book with the same ID already exists. If that is the case, we return the 409: Conflict error back.',
                    },
                    {
                        id: 2,
                        heading: '3.1.2 Get a single book',
                        text: 'Once we decode and validate the JSON body against our Book struct (if it fails we will return the 400: Bad Request error), we need to check if the book with the same ID already exists. If that is the case, we return the 409: Conflict error back.',
                        imgSrc: '/img/home-img.jpg',
                    },
                    {
                        id: 3,
                        heading: '3.1.3 Helpers',
                        text: 'Once we decode and validate the JSON body against our Book struct (if it fails we will return the 400: Bad Request error), we need to check if the book with the same ID already exists. If that is the case, we return the 409: Conflict error back.',
                    },
                ],
            },
        ],
    },
];

const Blog: React.FC = () => {
    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const [blogPost, setBlogPost] = useState<[] | BlogPostType[]>([]);

    const displayBlogPostItems = () => {
        return blogPost.map((blogItem: BlogPostType) => (
            <Box key={blogItem.id} mb={1}>
                <Typography variant='h4'>{blogItem.heading}</Typography>
                <Typography fontSize='1.3rem'>
                    {blogItem.headingText}
                </Typography>
            </Box>
        ));
    };

    return (
        <>
            {isAdminActive && (
                <Box
                    mt={2}
                    p={2}
                    border={1}
                    borderRadius={1}
                    borderColor='grey.400'
                >
                    {displayBlogPostItems()}

                    <HeadingOptionBtn btnType='add' setBlogPost={setBlogPost} />
                </Box>
            )}

            {/* Old code - will remove */}

            {/* {!isAdminActive && (
                <Container className={styles.blog}>
                    {blogPost2.map((item) => (
                        <Box key={item.id}>
                            <Typography variant='h2' mb={2}>
                                {item.heading}
                            </Typography>
                            <Typography
                                className={styles.blog__text}
                                variant='inherit'
                            >
                                {item.text}
                            </Typography>
                            {item.imgSrc && (
                                <Box className={styles['blog__img-box']}>
                                    <Image
                                        src={item.imgSrc}
                                        alt='home image'
                                        width={1000}
                                        height={400}
                                        objectFit='cover'
                                    />
                                </Box>
                            )}
                            {item.subtopics &&
                                item.subtopics.map((item) => (
                                    <Box key={item.id}>
                                        <Typography
                                            className={styles.blog__subheading}
                                            variant='h3'
                                        >
                                            {item.heading}
                                        </Typography>
                                        <Typography
                                            className={styles.blog__text}
                                            variant='inherit'
                                        >
                                            {item.text}
                                        </Typography>
                                        {item.imgSrc && (
                                            <Box
                                                className={
                                                    styles['blog__img-box']
                                                }
                                            >
                                                <Image
                                                    src={item.imgSrc}
                                                    alt='home image'
                                                    width={1000}
                                                    height={400}
                                                    objectFit='cover'
                                                />
                                            </Box>
                                        )}
                                        {item.subtopics &&
                                            item.subtopics.map((item) => (
                                                <Box key={item.id}>
                                                    <Typography
                                                        className={
                                                            styles.blog__subheading
                                                        }
                                                        variant='h3'
                                                    >
                                                        {item.heading}
                                                    </Typography>
                                                    <Typography
                                                        className={
                                                            styles.blog__text
                                                        }
                                                        variant='inherit'
                                                    >
                                                        {item.text}
                                                    </Typography>
                                                    {item.imgSrc && (
                                                        <Box
                                                            className={
                                                                styles[
                                                                    'blog__img-box'
                                                                ]
                                                            }
                                                        >
                                                            <Image
                                                                src={
                                                                    item.imgSrc
                                                                }
                                                                alt='home image'
                                                                width={1000}
                                                                height={400}
                                                                objectFit='cover'
                                                            />
                                                        </Box>
                                                    )}
                                                </Box>
                                            ))}
                                    </Box>
                                ))}
                        </Box>
                    ))}
                </Container>
            )} */}
        </>
    );
};

export default Blog;
