import React, { useMemo } from 'react';

import { disableSubmitOnEnterKey } from '../../../../utils/utils';

import styles from '../../../../styles/components/blog.module.scss';
import { TextField, Typography } from '@mui/material';

import { FieldValues } from 'react-hook-form';

export enum InputType {
    HEADING_INPUT = 'headingInput',
    HEADING_TEXT = 'headingText',
    SUBHEADING = 'subheading',
}

interface HeadingInputFieldProps extends FieldValues {
    inputType:
        | InputType.HEADING_INPUT
        | InputType.HEADING_TEXT
        | InputType.SUBHEADING;
}

enum InputLabel {
    heading = 'Heading',
    headingText = 'Text',
    subheading = 'Subheading',
}

const HeadingInputField: React.FC<HeadingInputFieldProps> = ({
    inputType,
    register,
    errors,
}) => {
    const setInputLabel = useMemo(() => {
        return inputType === InputType.HEADING_INPUT
            ? InputLabel.heading
            : inputType === InputType.HEADING_TEXT
            ? InputLabel.headingText
            : InputLabel.subheading;
    }, [inputType]);

    const setInputClassToApply = useMemo(
        () =>
            inputType === InputType.HEADING_INPUT
                ? styles['blog__new-heading']
                : null,
        [inputType],
    );

    const setInputOnKeyPress = useMemo(
        () =>
            inputType === InputType.HEADING_INPUT
                ? disableSubmitOnEnterKey
                : null,
        [inputType],
    );

    const setInputMultiline = useMemo(
        () =>
            inputType === InputType.HEADING_TEXT ||
            inputType === InputType.SUBHEADING,
        [inputType],
    );

    const setInputRows = useMemo(
        () => (inputType === InputType.HEADING_TEXT ? 4 : 2),
        [inputType],
    );

    return (
        <>
            <TextField
                className={setInputClassToApply}
                label={setInputLabel}
                name={inputType}
                {...register(inputType)}
                variant='outlined'
                color='primary'
                fullWidth
                InputProps={{
                    style: {
                        fontSize: 14,
                    },
                }}
                InputLabelProps={{
                    style: { fontSize: 14 },
                }}
                multiline={setInputMultiline}
                rows={setInputRows}
                onKeyPress={setInputOnKeyPress}
            />

            <Typography color='red' fontWeight='bold' mb='1rem'>
                {errors[`${inputType}`]?.message}
            </Typography>
        </>
    );
};

export default HeadingInputField;
