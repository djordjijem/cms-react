import React from 'react';
import { FaRegPaperPlane } from 'react-icons/fa';
import styles from '../../styles/components/newsletter.module.scss';
import {
    Typography,
    Box,
    TextField,
    InputAdornment,
    IconButton,
} from '@mui/material';

const Newsletter: React.FC = () => {
    return (
        <Box
            className={styles.newsletter}
            display='flex'
            alignItems='center'
            mt={2}
            padding={4}
            borderRadius={2}
        >
            <Typography
                className={styles.newsletter__para}
                fontSize={16}
                fontWeight='bold'
                color='grey.800'
                flex='40%'
            >
                Subscribe to my newsletter and never miss my upcoming articles
            </Typography>
            <Box flex='60%' width='100%'>
                <form noValidate autoComplete='off'>
                    <TextField
                        label='Email Adress'
                        variant='outlined'
                        color='primary'
                        fullWidth
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position='end'>
                                    <IconButton
                                        edge='end'
                                        color='primary'
                                        type='submit'
                                    >
                                        <FaRegPaperPlane size='2.3rem' />
                                    </IconButton>
                                </InputAdornment>
                            ),
                            style: {
                                fontSize: 18,
                                paddingRight: '2rem',
                            },
                        }}
                        InputLabelProps={{
                            style: { fontSize: 18 },
                        }}
                    />
                </form>
            </Box>
        </Box>
    );
};

export default Newsletter;
