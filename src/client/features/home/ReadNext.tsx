import React from 'react';
import { FaBookOpen } from 'react-icons/fa';
import styles from '../../styles/components/readNext.module.scss';
import {
    Typography,
    Box,
    Grid,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    Avatar,
} from '@mui/material';

const ReadNext: React.FC = () => {
    const readNextItems = [
        {
            id: 0,
            publisherSrc: 'img/publisher.jpg',
            name: 'vojko v',
            imgSrc: 'img/read-next-img.jpg',
            heading: 'Get started with asynchronous programming in Python',
            text: 'Heard of async but you never really understood what it is or why you should use it? Hopefully, this …',
            published: 'September 14, 2016',
        },
        {
            id: 1,
            publisherSrc: 'img/publisher.jpg',
            name: 'vojko v',
            imgSrc: 'img/read-next-img.jpg',
            heading: 'Build your question-answering Slack bot in Python',
            text: 'This post will help you create your own know-it-all Slack bot in Python in few very easy steps. The …',
            published: 'January 22, 2019',
        },
        {
            id: 2,
            publisherSrc: 'img/publisher.jpg',
            name: 'vojko v',
            imgSrc: 'img/read-next-img.jpg',
            heading: 'Create image recognition bot with Python',
            text: 'Building an image recognition bot can greatly help you offload your day-to-day manual work and save …',
            published: 'October 1, 2021',
        },
    ];

    return (
        <Box mt={5}>
            <Typography
                variant='h3'
                display='flex'
                alignItems='center'
                flexDirection='column'
                color='grey.800'
            >
                Read Next
                <FaBookOpen />
            </Typography>

            <Box mt={2}>
                <Grid container spacing={2}>
                    {readNextItems.map((item) => (
                        <Grid item key={item.id} xs={12} sm={6} md={4}>
                            <Card
                                className={styles['read-next__card']}
                                elevation={3}
                            >
                                <CardHeader
                                    className={styles['read-next__card-header']}
                                    avatar={
                                        <Avatar
                                            className={
                                                styles['read-next__card-avatar']
                                            }
                                            src={item.publisherSrc}
                                            alt='publisher'
                                        />
                                    }
                                    titleTypographyProps={{
                                        variant: 'h4',
                                        fontWeight: 600,
                                        color: 'grey.800',
                                    }}
                                    subheaderTypographyProps={{
                                        variant: 'h6',
                                        color: 'grey.800',
                                    }}
                                    title={item.name}
                                    subheader={item.published}
                                />
                                <CardMedia
                                    className={styles['read-next__card-image']}
                                    component='img'
                                    image={item.imgSrc}
                                    alt={item.heading}
                                />
                                <Typography
                                    variant='h5'
                                    mt={1}
                                    fontWeight={700}
                                >
                                    {item.heading}
                                </Typography>
                                <CardContent
                                    className={
                                        styles['read-next__card-content']
                                    }
                                >
                                    <Typography
                                        variant='body1'
                                        color='grey.800'
                                        fontSize='1.5rem'
                                    >
                                        {item.text}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Box>
        </Box>
    );
};

export default ReadNext;
