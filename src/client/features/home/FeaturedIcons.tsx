import React, { useState } from 'react';

import { FEATURED_ICONS } from '../../static';

import styles from '../../styles/components/featuredIcons.module.scss';
import {
    Typography,
    Box,
    Card,
    InputLabel,
    MenuItem,
    FormControl,
    ListItemText,
    Select,
    Checkbox,
} from '@mui/material';

import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';

const FeaturedIcons: React.FC = () => {
    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const [featuredOn, setFeaturedOn] = useState<string[]>(['GitHub']);

    const handleSelectChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;

        setFeaturedOn(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };

    return (
        <Box mt={2}>
            {isAdminActive && (
                <Box>
                    <FormControl className={styles['featured-icons__select']}>
                        <InputLabel id='demo-multiple-checkbox-label'>
                            <Typography fontSize='1.3rem'>
                                Featured On
                            </Typography>
                        </InputLabel>
                        <Select
                            labelId='demo-multiple-checkbox-label'
                            id='demo-multiple-checkbox'
                            multiple
                            value={featuredOn}
                            onChange={handleSelectChange}
                            label={<Typography>Featured On</Typography>}
                            renderValue={(selected) => selected.join(', ')}
                        >
                            {FEATURED_ICONS.map((icon) => (
                                <MenuItem key={icon.id} value={icon.text}>
                                    <Checkbox
                                        checked={
                                            featuredOn.indexOf(icon.text) > -1
                                        }
                                    />
                                    <ListItemText
                                        primary={
                                            <Typography fontSize='1.3rem'>
                                                {icon.text}
                                            </Typography>
                                        }
                                    />
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Box>
            )}

            {!isAdminActive && (
                <Box display='flex' flexWrap='wrap'>
                    {FEATURED_ICONS.filter(
                        (icon) => icon.text === featuredOn[0] || featuredOn[1],
                    ).map((item) => (
                        <Card
                            className={styles['featured-icons__icon']}
                            key={item.id}
                            elevation={0}
                        >
                            <Box
                                className={styles['featured-icons__icon-img']}
                                display='flex'
                                justifyContent='center'
                                alignItems='center'
                                p={1.5}
                            >
                                {item.icon}
                            </Box>

                            <Typography
                                p={1.5}
                                fontSize='1.4rem'
                                fontWeight='bold'
                                letterSpacing={0.3}
                            >
                                {`Featured On ${item.text}`}
                            </Typography>
                        </Card>
                    ))}
                </Box>
            )}
        </Box>
    );
};

export default FeaturedIcons;
