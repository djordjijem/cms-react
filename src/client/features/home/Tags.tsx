import React, { useState } from 'react';

import styles from '../../styles/components/tags.module.scss';
import { Box, IconButton, TextField, Typography, Badge } from '@mui/material';
import { AiFillPlusCircle, AiOutlineClose } from 'react-icons/ai';

import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';

import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import { newTagSchema } from '../../joi-schemas/joiSchemas';

import { v4 as uuidv4 } from 'uuid';

const Tags: React.FC = () => {
    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const [tags, setTags] = useState([{ id: uuidv4(), tag: 'tag' }]);

    const displayTags = () => {
        return tags.map((item) => (
            <Box
                className={styles.tags__tag}
                key={item.id}
                padding={1}
                mr={1}
                border={1}
                borderColor='primary.light'
                borderRadius='0.7rem'
                fontSize='2rem'
                color='grey.800'
            >
                #{item.tag}
            </Box>
        ));
    };

    const displayTagsWithBadge = () => {
        return tags.map((item) => (
            <Badge
                className={styles.tags__badge}
                onClick={() => handleRemoveTag(item.id)}
                key={item.id}
                badgeContent={<AiOutlineClose size='1rem' />}
                color='primary'
            >
                <Box
                    className={styles.tags__tag}
                    padding={1}
                    border={1}
                    borderColor='primary.light'
                    borderRadius='0.7rem'
                    fontSize='2rem'
                    color='grey.800'
                >
                    #{item.tag}
                </Box>
            </Badge>
        ));
    };

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        setError,
        clearErrors,
    } = useForm({
        resolver: joiResolver(newTagSchema),
    });

    const addNewTag = (data: { newTag: string }) => {
        const { newTag } = data;

        const isExist = tags.some((tag) => tag.tag === newTag);

        if (isExist) {
            setError('duplicateError', { message: 'Duplicate tag name!' });

            setTimeout(() => {
                clearErrors();
            }, 1000);

            return;
        }

        setTags((prevState) => [...prevState, { id: uuidv4(), tag: newTag }]);

        reset();
    };

    const handleRemoveTag = (id: string) => {
        const tagsArray = [...tags];

        const filteredTagsArray = tagsArray.filter((tag) => tag.id !== id);

        setTags(filteredTagsArray);
    };

    return (
        <Box mt={2}>
            {isAdminActive && (
                <Box display='flex' alignItems='center' flexWrap='wrap'>
                    {displayTagsWithBadge()}

                    <form
                        onSubmit={handleSubmit(addNewTag)}
                        noValidate
                        autoComplete='off'
                    >
                        <TextField
                            variant='outlined'
                            color='primary'
                            InputProps={{
                                style: {
                                    fontSize: 14,
                                },
                            }}
                            name='newTag'
                            {...register('newTag')}
                        />

                        <IconButton size='medium' color='primary' type='submit'>
                            <AiFillPlusCircle size='3rem' />
                        </IconButton>

                        <Typography
                            color='red'
                            fontSize='1.1rem'
                            fontWeight='bold'
                        >
                            {errors.newTag?.message}
                            {errors.duplicateError?.message}
                        </Typography>
                    </form>
                </Box>
            )}

            {!isAdminActive && (
                <Box display='flex' alignItems='center' flexWrap='wrap'>
                    {displayTags()}
                </Box>
            )}
        </Box>
    );
};

export default Tags;
