import React from 'react';

import { NextPage } from 'next';

import Title from './Title';
import MainImage from './MainImage';
import Publisher from './Publisher';
import Newsletter from './Newsletter';
import FeaturedIcons from './FeaturedIcons';
import BlogIntro from './BlogIntro';
import Blog from './blog/Blog';
import Tags from './Tags';
import ReadNext from './ReadNext';
import Comments from '../comments/Comments';

import { Container } from '@mui/material';

const HomePage: NextPage = () => {
    return (
        <Container>
            <Title />
            <MainImage />
            <FeaturedIcons />
            <Publisher />
            <Newsletter />
            <BlogIntro />
            <Blog />
            <Tags />
            <ReadNext />
            <Comments />
        </Container>
    );
};

export default HomePage;
