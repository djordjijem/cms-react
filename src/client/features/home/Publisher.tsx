import React, { useState } from 'react';

import { PUBLISHERS } from '../../static';

import styles from '../../styles/components/publisher.module.scss';
import {
    Typography,
    Box,
    Avatar,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
} from '@mui/material';

import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';

import { format } from 'date-fns';

const Publisher: React.FC = () => {
    interface PublisherInfo {
        publisher: string;
        datePublished: string | null;
    }

    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const [publisherInfo, setPublisherInfo] = useState<PublisherInfo>({
        publisher: 'Frank E Tovar',
        datePublished: null,
    });

    const handlePublisherChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const date = format(new Date(), 'MMM dd, yyyy');

        const publisher = e.target.value;

        setPublisherInfo((prevState) => ({
            ...prevState,
            publisher,
            datePublished: date,
        }));
    };

    const findPublisher = (selectedPublisher: string) =>
        PUBLISHERS.find((publisher) => publisher.name === selectedPublisher);

    const { img: publisherImage, name: publisherName } = findPublisher(
        publisherInfo.publisher,
    );

    return (
        <Box mt={2}>
            {isAdminActive && (
                <Box>
                    <FormControl className={styles.publisher__select}>
                        <InputLabel id='demo-simple-select-label'>
                            <Typography fontSize='1.3rem'>Publisher</Typography>
                        </InputLabel>
                        <Select
                            labelId='demo-simple-select-label'
                            id='demo-simple-select'
                            value={publisherInfo.publisher}
                            label={<Typography>Publisher</Typography>}
                            onChange={handlePublisherChange}
                        >
                            {PUBLISHERS.map((publisher) => (
                                <MenuItem
                                    key={publisher.id}
                                    value={publisher.name}
                                >
                                    {publisher.name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Box>
            )}

            {!isAdminActive && (
                <Box
                    display='flex'
                    alignItems='center'
                    pt={3}
                    pb={3}
                    border={1}
                    borderLeft={0}
                    borderRight={0}
                    borderColor='grey.400'
                >
                    <Avatar
                        className={styles.publisher__avatar}
                        src={publisherImage}
                        alt='publisher'
                    />

                    <Box color='grey.800'>
                        <Typography
                            className={styles.publisher__name}
                            variant='h4'
                            mb={0.5}
                            fontWeight={700}
                        >
                            {publisherName}
                        </Typography>

                        <Typography mb={0.5} fontSize={14}>
                            Published on{' '}
                            <Typography
                                component='span'
                                display='inline-block'
                                fontWeight='bold'
                                fontSize={14}
                            >
                                {publisherInfo.datePublished}
                            </Typography>
                        </Typography>
                    </Box>
                </Box>
            )}
        </Box>
    );
};

export default Publisher;
