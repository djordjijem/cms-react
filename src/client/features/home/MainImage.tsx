import React, { useRef, useState } from 'react';

import Image from 'next/image';

import styles from '../../styles/components/mainImage.module.scss';
import { Box, IconButton, Input } from '@mui/material';
import Zoom from '@mui/material/Zoom';
import { IoIosResize } from 'react-icons/io';
import { FcRemoveImage, FcAddImage } from 'react-icons/fc';

import { RootState } from '../../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { mainImageSlice } from '../../redux/slices/mainImageSlice';

import ImageCropper from '../image-cropper/ImageCropper';

const MainImage: React.FC = () => {
    const [isResizeOpen, setIsResizeOpen] = useState(false);

    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const mainImage = useSelector(
        (state: RootState) => state.mainImage.mainImage,
    );

    const mainImageCopy = useSelector(
        (state: RootState) => state.mainImage.mainImageCopy,
    );

    const croppedMainImage = useSelector(
        (state: RootState) => state.mainImage.croppedMainImage,
    );

    const imageInputRef = useRef<HTMLInputElement | null>(null);

    const IS_ADMIN_ACTIVE_AND_MAIN_IMAGE = !isAdminActive && mainImage;

    const dispatch = useDispatch();

    const handleAddImageClick = () => imageInputRef.current.click();

    const handleRemoveImageClick = () => {
        dispatch(mainImageSlice.actions.resetMainImageState());
        dispatch(mainImageSlice.actions.resetCroppedMainImageState());

        imageInputRef.current.value = null;
    };

    const handleResizeClick = () => {
        if (mainImage) {
            setIsResizeOpen((isResizeOpen) => !isResizeOpen);

            dispatch(mainImageSlice.actions.updateMainImage(mainImageCopy));
            dispatch(mainImageSlice.actions.resetCroppedMainImageState());
        }

        if (croppedMainImage)
            dispatch(mainImageSlice.actions.updateMainImage(croppedMainImage));
    };

    const handleImageInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files[0];

        if (file?.type.slice(0, 5) === 'image') {
            const reader = new FileReader();

            reader.onloadend = () => {
                dispatch(
                    mainImageSlice.actions.updateMainImage(
                        reader.result as string,
                    ),
                );

                dispatch(
                    mainImageSlice.actions.updateMainImageCopy(
                        reader.result as string,
                    ),
                );

                dispatch(mainImageSlice.actions.resetCroppedMainImageState());
            };

            reader.readAsDataURL(file);
        } else {
            dispatch(mainImageSlice.actions.resetMainImageState());
        }
    };

    const showCropperOrImage = () => {
        return isResizeOpen ? (
            <ImageCropper />
        ) : (
            <Image
                src={mainImage}
                alt='home image'
                width={1000}
                height={400}
                objectFit='cover'
            />
        );
    };

    return (
        <>
            <Box display='flex'>
                {isAdminActive && (
                    <>
                        <Box
                            className={styles['main-image__box']}
                            flex='95%'
                            border={isAdminActive && 1}
                            borderRadius={1}
                            borderColor='grey.400'
                        >
                            {mainImage && showCropperOrImage()}

                            {!mainImage && (
                                <Image
                                    src={'/img/no-photo-img.png'}
                                    alt='home image'
                                    width={1000}
                                    height={400}
                                    objectFit='cover'
                                />
                            )}

                            <Input
                                className={styles['main-image__file-input']}
                                onChange={handleImageInputChange}
                                inputRef={imageInputRef}
                                type='file'
                                inputProps={{ accept: 'image/*' }}
                            />
                        </Box>

                        <Box
                            flex='5%'
                            display='flex'
                            flexDirection='column'
                            alignItems='end'
                        >
                            <Zoom in>
                                <Box>
                                    <IconButton
                                        onClick={handleAddImageClick}
                                        size='large'
                                        color='primary'
                                    >
                                        <FcAddImage size='2.2rem' />
                                    </IconButton>
                                </Box>
                            </Zoom>

                            <Zoom in>
                                <Box>
                                    <IconButton
                                        onClick={handleRemoveImageClick}
                                        size='large'
                                        color='primary'
                                    >
                                        <FcRemoveImage size='2.2rem' />
                                    </IconButton>
                                </Box>
                            </Zoom>

                            <Zoom in>
                                <Box>
                                    <IconButton
                                        onClick={handleResizeClick}
                                        size='large'
                                        color='primary'
                                    >
                                        <IoIosResize size='2.2rem' />
                                    </IconButton>
                                </Box>
                            </Zoom>
                        </Box>
                    </>
                )}

                {IS_ADMIN_ACTIVE_AND_MAIN_IMAGE && (
                    <Image
                        src={mainImage}
                        alt='home image'
                        width={1000}
                        height={400}
                        objectFit='cover'
                    />
                )}
            </Box>
        </>
    );
};

export default MainImage;
