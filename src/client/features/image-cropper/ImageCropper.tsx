import React, { useRef } from 'react';

import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import { mainImageSlice } from '../../redux/slices/mainImageSlice';

const ImageCropper: React.FC = () => {
    const mainImage = useSelector(
        (state: RootState) => state.mainImage.mainImage,
    );

    const cropperRef = useRef<HTMLImageElement>(null);

    const dispatch = useDispatch();

    const onCrop = () => {
        const imageElement: any = cropperRef?.current;
        const cropper: any = imageElement?.cropper;
        const croppedImage = cropper.getCroppedCanvas().toDataURL();

        dispatch(mainImageSlice.actions.updateCroppedMainImage(croppedImage));
    };

    return (
        <Cropper
            src={mainImage}
            style={{ height: 400, width: '100%' }}
            // Cropper.js options
            initialAspectRatio={16 / 9}
            guides={false}
            crop={onCrop}
            ref={cropperRef}
        />
    );
};

export default ImageCropper;
