import React, { useState } from 'react';

import ReplyInputField from './comment-reply/reply-input/ReplyInputField';
import CustomButton from '../../../components/button/CustomButton';
import ReplyAddBtn from './comment-reply/reply-button/ReplyAddBtn';
import CommentReply from './comment-reply/CommentReply';

import { COMMENT_BTNS } from '../../../static';

import styles from '../../../styles/components/comment.module.scss';
import {
    Typography,
    Box,
    Card,
    CardHeader,
    Avatar,
    CardContent,
} from '@mui/material';

import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import { newReplySchema } from '../../../joi-schemas/joiSchemas';

import { v4 as uuidv4 } from 'uuid';

import { format } from 'date-fns';

import { CommentType } from '../Comments';
import { CommentActions } from '../reducer/commentsReducer';

interface CommentProps {
    comment: CommentType;
    dispatch: React.Dispatch<{
        type: string;
        value: { id: string; reply?: ReplyType };
    }>;
}

export interface ReplyType {
    id: string;
    postedBy: {
        name: string;
        job: string;
        img: string;
    };
    datePosted: string;
    commentText: string;
    likes: number;
}

const Comment: React.FC<CommentProps> = ({ comment, dispatch }) => {
    const { id, postedBy, datePosted, commentText, likes, replies } = comment;
    const { name, job, img } = postedBy;

    const [isReplying, setIsReplying] = useState(false);

    const [isLiked, setIsLiked] = useState(false);

    const { register, handleSubmit, reset } = useForm({
        resolver: joiResolver(newReplySchema),
    });

    const addNewReply = (data: { newReply: string }) => {
        const replyId = uuidv4();
        const datePosted = format(new Date(), 'MMM dd');
        const commentText = data.newReply;

        const reply: ReplyType = {
            id: replyId,
            postedBy: {
                name: 'Replier Y',
                job: 'CEO',
                img: 'img/publisher/publisher1.jpg',
            },
            datePosted,
            commentText,
            likes: 0,
        };

        dispatch({ type: CommentActions.ADD_REPLY, value: { id, reply } });

        reset();
    };

    const handleReplyClick = () => setIsReplying((prevState) => !prevState);

    const handleLikeClick = () => {
        if (!isLiked) {
            dispatch({ type: CommentActions.LIKE_COMMENT, value: { id } });
            setIsLiked(true);
            return;
        }

        dispatch({ type: CommentActions.UNLIKE_COMMENT, value: { id } });
        setIsLiked(false);
    };

    return (
        <Card className={styles.comment} elevation={3}>
            <CardHeader
                className={styles.comment__header}
                avatar={
                    <Avatar
                        className={styles.comment__avatar}
                        src={img}
                        alt='Posted by'
                    />
                }
                title={
                    <Typography
                        variant='h5'
                        color='primary.main'
                        fontWeight={700}
                    >
                        {name}
                    </Typography>
                }
                subheader={
                    <Typography
                        className={styles.comment__job}
                        variant='h6'
                        color='grey.600'
                        fontWeight={600}
                    >
                        {job}
                    </Typography>
                }
                action={
                    <Typography
                        fontSize='1.5rem'
                        color='primary.main'
                        fontWeight={600}
                    >
                        {datePosted}
                    </Typography>
                }
            />

            <CardContent className={styles.comment__content}>
                <Typography variant='body2' color='grey.800' fontSize='1.7rem'>
                    {commentText}
                </Typography>
            </CardContent>

            <Typography color='primary' fontSize='1.5rem' fontWeight={700}>
                {likes}
            </Typography>

            <Box display='flex' justifyContent='space-between'>
                {COMMENT_BTNS.map((item) => (
                    <CustomButton
                        className={styles.comment__btn}
                        key={item.id}
                        onClick={
                            item.id === 0 ? handleLikeClick : handleReplyClick
                        }
                        startIcon={isLiked ? item.icon_2 : item.icon}
                        text={item.text}
                    />
                ))}
            </Box>

            {replies?.map((reply) => (
                <CommentReply
                    key={reply.id}
                    reply={reply}
                    dispatch={dispatch}
                    commentId={id}
                />
            ))}

            {isReplying && (
                <Box mt={1}>
                    <form
                        onSubmit={handleSubmit(addNewReply)}
                        noValidate
                        autoComplete='off'
                    >
                        <ReplyInputField register={register} name={name} />
                        <ReplyAddBtn />
                    </form>
                </Box>
            )}
        </Card>
    );
};

export default Comment;
