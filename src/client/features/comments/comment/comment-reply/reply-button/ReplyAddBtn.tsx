import React from 'react';

import CustomButton from '../../../../../components/button/CustomButton';

import styles from '../../../../../styles/components/comments.module.scss';
import { Box } from '@mui/material';
import { FaPencilAlt } from 'react-icons/fa';

const ReplyAddBtn: React.FC = () => {
    return (
        <Box mt={1} display='flex' justifyContent='right'>
            <CustomButton
                className={styles['comments__add-comment-btn']}
                variant='outlined'
                startIcon={<FaPencilAlt size='1.5rem' />}
                text='Add a reply'
                type='submit'
            />
        </Box>
    );
};

export default ReplyAddBtn;
