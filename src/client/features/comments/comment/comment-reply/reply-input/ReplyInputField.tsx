import React from 'react';

import { disableSubmitOnEnterKey } from '../../../../../utils/utils';

import { Box, TextField } from '@mui/material';

import { FieldValues } from 'react-hook-form';

interface ReplyInputFieldProps extends FieldValues {
    name: string;
}

const ReplyInputField: React.FC<ReplyInputFieldProps> = ({
    register,
    name,
}) => {
    return (
        <Box mt={3}>
            <TextField
                name='newReply'
                {...register('newReply')}
                label={`Reply to ${name}`}
                variant='outlined'
                color='primary'
                fullWidth
                InputProps={{
                    style: {
                        fontSize: 14,
                    },
                }}
                InputLabelProps={{
                    style: { fontSize: 14 },
                }}
                onKeyPress={disableSubmitOnEnterKey}
            />
        </Box>
    );
};

export default ReplyInputField;
