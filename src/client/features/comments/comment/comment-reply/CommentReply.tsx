import React, { useState } from 'react';

import CustomButton from '../../../../components/button/CustomButton';

import { COMMENT_BTNS } from '../../../../static';

import styles from '../../../../styles/components/comment.module.scss';
import {
    Typography,
    Card,
    CardHeader,
    Avatar,
    CardContent,
    Box,
} from '@mui/material';

import { CommentActions } from '../../reducer/commentsReducer';
import { ReplyType } from '../Comment';

interface CommentReplyProps {
    reply: ReplyType;
    dispatch: React.Dispatch<{
        type: string;
        value: { id: string; replyId: string };
    }>;
    commentId: string;
}

const CommentReply: React.FC<CommentReplyProps> = ({
    reply,
    dispatch,
    commentId,
}) => {
    const { id: replyId, postedBy, datePosted, commentText, likes } = reply;
    const { name, job, img } = postedBy;

    const [isLiked, setIsLiked] = useState(false);

    const handleLikeClick = () => {
        if (!isLiked) {
            dispatch({
                type: CommentActions.LIKE_REPLY,
                value: { id: commentId, replyId },
            });
            setIsLiked(true);
            return;
        }

        dispatch({
            type: CommentActions.UNLIKE_REPLY,
            value: { id: commentId, replyId },
        });
        setIsLiked(false);
    };

    return (
        <Card
            className={`${styles.comment} ${styles['comment-reply']}`}
            elevation={3}
        >
            <CardHeader
                className={styles.comment__header}
                avatar={
                    <Avatar
                        className={styles.comment__avatar}
                        src={img}
                        alt='Posted by'
                    />
                }
                title={
                    <Typography
                        variant='h5'
                        color='primary.main'
                        fontWeight={700}
                    >
                        {name}
                    </Typography>
                }
                subheader={
                    <Typography
                        className={styles.comment__job}
                        variant='h6'
                        color='grey.600'
                        fontWeight={600}
                    >
                        {job}
                    </Typography>
                }
                action={
                    <Typography
                        fontSize='1.5rem'
                        color='primary.main'
                        fontWeight={600}
                    >
                        {datePosted}
                    </Typography>
                }
            />

            <CardContent className={styles.comment__content}>
                <Typography display='block' color='grey.800' fontSize='1.7rem'>
                    {commentText}
                </Typography>
            </CardContent>

            <Typography color='primary' fontSize='1.5rem' fontWeight={700}>
                {likes}
            </Typography>

            <Box>
                {COMMENT_BTNS.map((item) => {
                    if (item.id === 0) {
                        return (
                            <CustomButton
                                className={styles.comment__btn}
                                onClick={handleLikeClick}
                                key={item.id}
                                startIcon={isLiked ? item.icon_2 : item.icon}
                                text={item.text}
                            />
                        );
                    }
                })}
            </Box>
        </Card>
    );
};

export default CommentReply;
