import React from 'react';

import { ReplyType } from '../comment/Comment';
import { CommentType } from '../Comments';

interface CommentsActionType {
    type: string;
    value: {
        comment?: CommentType;
        id?: string;
        reply?: ReplyType;
        replyId?: string;
    };
}

export enum CommentActions {
    ADD_COMMENT = 'ADD_NEW_COMMENT',
    LIKE_COMMENT = 'LIKE_COMMENT',
    UNLIKE_COMMENT = 'UNLIKE_COMMENT',
    ADD_REPLY = 'ADD_NEW_REPLY',
    LIKE_REPLY = 'LIKE_REPLY',
    UNLIKE_REPLY = 'UNLIKE_REPLY',
}

export const commentsInitialState: CommentType[] = [];

export const commentsReducer = (
    state: CommentType[],
    action: CommentsActionType,
) => {
    const { type, value } = action;
    const { id, reply, replyId } = value;

    switch (type) {
        case CommentActions.ADD_COMMENT:
            return [...state, value];
        case CommentActions.LIKE_COMMENT:
            return state.map((comment) =>
                comment.id === id
                    ? { ...comment, likes: comment.likes + 1 }
                    : comment,
            );
        case CommentActions.UNLIKE_COMMENT:
            return state.map((comment) =>
                comment.id === id
                    ? { ...comment, likes: comment.likes - 1 }
                    : comment,
            );
        case CommentActions.ADD_REPLY:
            return state.map((comment) =>
                comment.id === id
                    ? { ...comment, replies: [...comment.replies, reply] }
                    : comment,
            );
        case CommentActions.LIKE_REPLY:
            return state.map((comment) =>
                comment.id === id
                    ? {
                          ...comment,
                          replies: comment.replies.map((reply) =>
                              reply.id === replyId
                                  ? { ...reply, likes: reply.likes + 1 }
                                  : reply,
                          ),
                      }
                    : comment,
            );
        case CommentActions.UNLIKE_REPLY:
            return state.map((comment) =>
                comment.id === id
                    ? {
                          ...comment,
                          replies: comment.replies.map((reply) =>
                              reply.id === replyId
                                  ? { ...reply, likes: reply.likes - 1 }
                                  : reply,
                          ),
                      }
                    : comment,
            );
        default:
            return state;
    }
};
