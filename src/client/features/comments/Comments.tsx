import React, { useReducer } from 'react';

import CommentsHeader from './comments-header/CommentsHeader';
import CommentsInputField from './comments-inputs/CommentsInputField';
import CommentsAddBtn from './comments-buttons/CommentsAddBtn';
import Comment from './comment/Comment';

import { Box } from '@mui/material';

import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';

import { useForm } from 'react-hook-form';
import { joiResolver } from '@hookform/resolvers/joi';
import { newCommentSchema } from '../../joi-schemas/joiSchemas';

import { v4 as uuidv4 } from 'uuid';

import { format } from 'date-fns';

import { ReplyType } from './comment/Comment';
import { CommentActions } from './reducer/commentsReducer';

import {
    commentsReducer,
    commentsInitialState,
} from './reducer/commentsReducer';

export interface CommentType extends ReplyType {
    replies: ReplyType[];
}

const Comments: React.FC = () => {
    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const [comments, dispatch] = useReducer(
        commentsReducer,
        commentsInitialState,
    );

    const totalComments = comments.length;

    const { register, handleSubmit, reset } = useForm({
        resolver: joiResolver(newCommentSchema),
    });

    const addNewComment = (data: { newComment: string }) => {
        const id = uuidv4();
        const datePosted = format(new Date(), 'MMM dd');
        const commentText = data.newComment;

        const comment: CommentType = {
            id,
            postedBy: {
                name: 'publisher x',
                job: 'software developer',
                img: 'img/publisher/publisher2.jpg',
            },
            datePosted,
            commentText,
            likes: 0,
            replies: [],
        };

        dispatch({ type: CommentActions.ADD_COMMENT, value: comment });

        reset();
    };

    return (
        !isAdminActive && (
            <Box margin='5rem auto 3rem auto' maxWidth={800}>
                <CommentsHeader totalComments={totalComments} />

                {comments.map((comment: CommentType) => (
                    <Comment
                        key={comment.id}
                        comment={comment}
                        dispatch={dispatch}
                    />
                ))}

                <form
                    onSubmit={handleSubmit(addNewComment)}
                    noValidate
                    autoComplete='off'
                >
                    <CommentsInputField register={register} />

                    <CommentsAddBtn />
                </form>
            </Box>
        )
    );
};

export default Comments;
