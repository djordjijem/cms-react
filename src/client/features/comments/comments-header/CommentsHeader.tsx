import React from 'react';

import CommentsHeaderDropdownBtn from '../comments-buttons/CommentsHeaderDropdownBtn';

import { Typography, Box } from '@mui/material';

interface CommentsHeaderProps {
    totalComments: number;
}

const CommentsHeader: React.FC<CommentsHeaderProps> = ({ totalComments }) => {
    return (
        <Box
            mb={2}
            p={2}
            border={1}
            borderColor='grey.400'
            borderRadius='0.7rem'
            display='flex'
            alignItems='center'
            justifyContent='space-between'
            flexWrap='wrap'
        >
            <Typography variant='h5' component='p' mr={3} fontWeight='bold'>
                Comments ({totalComments})
            </Typography>

            <CommentsHeaderDropdownBtn />
        </Box>
    );
};

export default CommentsHeader;
