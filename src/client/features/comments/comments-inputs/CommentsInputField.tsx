import React from 'react';

import { Box, TextField } from '@mui/material';

import { FieldValues } from 'react-hook-form';

const CommentsInputField: React.FC<FieldValues> = ({ register }) => {
    return (
        <Box mb={1} mt={2}>
            <TextField
                name='newComment'
                {...register('newComment')}
                label='Comment'
                variant='outlined'
                color='primary'
                fullWidth
                InputProps={{
                    style: {
                        fontSize: 14,
                    },
                }}
                InputLabelProps={{
                    style: { fontSize: 14 },
                }}
                multiline
                rows={4}
            />
        </Box>
    );
};

export default CommentsInputField;
