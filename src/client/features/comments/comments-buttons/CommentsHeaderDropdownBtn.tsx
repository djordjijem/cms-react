import React, { useState } from 'react';

import CustomButton from '../../../components/button/CustomButton';

import { COMMENTS_HEADER_DROPDOWN_ITEMS } from '../../../static';

import styles from '../../../styles/components/comments.module.scss';
import { Menu, MenuItem } from '@mui/material';
import { IoIosArrowDown } from 'react-icons/io';

const CommentsHeaderDropdownBtn: React.FC = () => {
    const [anchorEl, setAnchorEl] = useState(null);
    const isOpen = Boolean(anchorEl);

    const handleDropdownClick = (e: { currentTarget: EventTarget }) =>
        setAnchorEl(e.currentTarget);

    const handleDropdownClose = () => setAnchorEl(null);

    return (
        <>
            <CustomButton
                className={styles['comments__dropdown-btn']}
                onClick={handleDropdownClick}
                id='basic-button'
                aria-controls='basic-menu'
                aria-haspopup='true'
                aria-expanded={isOpen ? 'true' : undefined}
                endIcon={<IoIosArrowDown size='2.2rem' />}
                text='Popular'
            />

            <Menu
                open={isOpen}
                onClose={handleDropdownClose}
                id='basic-menu'
                anchorEl={anchorEl}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                {COMMENTS_HEADER_DROPDOWN_ITEMS.map((item) => (
                    <MenuItem
                        className={styles['comments__dropdown-item']}
                        key={item.id}
                    >
                        {item.text}
                    </MenuItem>
                ))}
            </Menu>
        </>
    );
};

export default CommentsHeaderDropdownBtn;
