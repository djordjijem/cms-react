import {
    BsEmojiSmile,
    BsEmojiSmileFill,
    BsFillReplyAllFill,
    BsGithub,
} from 'react-icons/bs';
import { FaPowerOff } from 'react-icons/fa';
import { SiHashnode } from 'react-icons/si';

/* AppBar */

export const APPBAR_UP_ITEMS = [{ id: 0, icon: <FaPowerOff /> }];

export const APPBAR_DOWN_ITEMS = [{ id: 0, text: 'admin' }];

/* End of AppBar */

/* FeaturedIcons */

export const FEATURED_ICONS = [
    { id: 0, text: 'Hashnode', icon: <SiHashnode size='2rem' /> },
    { id: 1, text: 'GitHub', icon: <BsGithub size='2rem' /> },
];

/* End of FeaturedIcons */

/* Publisher */

export const PUBLISHERS = [
    { id: 0, name: 'Frank E Tovar', img: 'img/publisher/publisher1.jpg' },
    { id: 1, name: 'Kevin Holmes', img: 'img/publisher/publisher2.jpg' },
    { id: 2, name: 'Mark Packer', img: 'img/publisher/publisher3.jpg' },
];

/* End of Publisher */

/* Comments */

export const COMMENTS_HEADER_DROPDOWN_ITEMS = [
    { id: 0, text: 'Popular' },
    { id: 1, text: 'Recent' },
];

/* End of Comments */

/* Comment */

export const COMMENT_BTNS = [
    {
        id: 0,
        icon: <BsEmojiSmile />,
        icon_2: <BsEmojiSmileFill />,
        text: 'like',
    },
    {
        id: 1,
        icon: <BsFillReplyAllFill />,
        icon_2: <BsFillReplyAllFill />,
        text: 'reply',
    },
];

/* End of Comment */

/* Footer */

export const footerLinks = [
    { id: 0, text: 'privacy', link: '#' },
    { id: 1, text: 'terms', link: '#' },
];

/* End of Footer */
