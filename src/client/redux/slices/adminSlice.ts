import { createSlice } from '@reduxjs/toolkit';

export interface IsAdminActiveState {
    isAdminActive: boolean;
}

const initialState: IsAdminActiveState = {
    isAdminActive: false,
};

const name = 'isAdminActive';

export const adminSlice = createSlice({
    name,
    initialState,
    reducers: {
        setIsAdminActive: (state) => {
            state.isAdminActive = !state.isAdminActive;
        },
    },
});

export const { setIsAdminActive } = adminSlice.actions;

export default adminSlice.reducer;
