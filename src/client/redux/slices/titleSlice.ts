import { createSlice } from '@reduxjs/toolkit';

export interface TitleState {
    title: string;
}

type UpdateTitleAction = {
    payload: string;
};

const initialState: TitleState = {
    title: 'Title text...',
};

const name = 'title';

export const titleSlice = createSlice({
    name,
    initialState,
    reducers: {
        updateTitle: (state, action: UpdateTitleAction) => {
            state.title = action.payload;
        },
    },
});

export const { updateTitle } = titleSlice.actions;

export default titleSlice.reducer;
