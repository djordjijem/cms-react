import { createSlice } from '@reduxjs/toolkit';

export interface MainImageState {
    mainImage: string | null;
    mainImageCopy: string | null;
    croppedMainImage: string | null;
}

type UpdateMainImageAction = {
    payload: string;
};

const initialState: MainImageState = {
    mainImage: '/img/no-photo-img.png',
    mainImageCopy: null,
    croppedMainImage: null,
};

const name = 'mainImage';

export const mainImageSlice = createSlice({
    name,
    initialState,
    reducers: {
        updateMainImage: (state, action: UpdateMainImageAction) => {
            state.mainImage = action.payload;
        },
        resetMainImageState: (state) => {
            state.mainImage = null;
        },
        updateMainImageCopy: (state, action: UpdateMainImageAction) => {
            state.mainImageCopy = action.payload;
        },
        updateCroppedMainImage: (state, action: UpdateMainImageAction) => {
            state.croppedMainImage = action.payload;
        },
        resetCroppedMainImageState: (state) => {
            state.croppedMainImage = null;
        },
    },
});

export const { updateMainImage, resetMainImageState, updateMainImageCopy, updateCroppedMainImage, resetCroppedMainImageState } = mainImageSlice.actions;
 
export default mainImageSlice.reducer;
