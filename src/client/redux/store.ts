import { configureStore } from '@reduxjs/toolkit';
import adminSlice from './slices/adminSlice';
import titleSlice from './slices/titleSlice';
import mainImageSlice from './slices/mainImageSlice';

export const store = configureStore({
    reducer: {
        isAdminActive: adminSlice,
        title: titleSlice,
        mainImage: mainImageSlice,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
