import { createTheme } from '@mui/material';

export const THEME = createTheme({
    typography: {
        fontFamily: 'Quicksand',
        h1: {
            fontWeight: 500,
            fontSize: '4rem',
        },
        h2: {
            fontWeight: 500,
        },
        h3: {
            fontWeight: 500,
        },
        h4: {
            fontWeight: 500,
        },
        h5: {
            fontWeight: 500,
        },
    },
    palette: {
        primary: {
            main: '#3e2387',
            light: '#dad7de',
        },
        grey: {
            800: '#333',
            A100: '#ffffff4d',
            A200: '#ffffffd9',
        },
    },
});
