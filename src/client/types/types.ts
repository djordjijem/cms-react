export interface BlogPostType {
    id: string;
    heading: string;
    headingText: string;
}
