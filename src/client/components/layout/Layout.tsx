import Footer from '../footer/Footer';
import Navbar from '../navbar/Navbar';
import { ThemeProvider, Box } from '@mui/material';
import { THEME } from '../../styles/theme';

const Layout = ({ children }) => {
    return (
        <ThemeProvider theme={THEME}>
            <Navbar />
            <Box maxWidth='1000px' margin='0 auto'>
                {children}
            </Box>
            <Footer />
        </ThemeProvider>
    );
};

export default Layout;
