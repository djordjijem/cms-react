import React from 'react';

import { Button, ButtonProps } from '@mui/material';

interface CustomButtonProps extends ButtonProps {
    text: string;
}

const CustomButton: React.FC<CustomButtonProps> = ({
    className,
    startIcon,
    endIcon,
    onClick,
    color,
    variant,
    text,
    type,
}) => {
    return (
        <Button
            className={className}
            startIcon={startIcon}
            endIcon={endIcon}
            onClick={onClick}
            color={color}
            variant={variant}
            type={type}
        >
            {text}
        </Button>
    );
};

export default CustomButton;
