import React from 'react';
import { footerLinks } from '../../static';
import styles from '../../styles/layout/footer.module.scss';
import { Typography, Box, Link } from '@mui/material';

const Footer: React.FC = () => {
    const currentYear = new Date().getFullYear();

    return (
        <footer>
            <Box
                className={styles.footer}
                display='flex'
                flexDirection='column'
                justifyContent='center'
                alignItems='center'
                padding={5}
            >
                <Box display='flex'>
                    {footerLinks.map((item) => (
                        <Link
                            className={styles.footer__link}
                            href={item.link}
                            key={item.id}
                            color='primary.light'
                            fontSize='1.5rem'
                            fontWeight='bold'
                            underline='none'
                            padding={1}
                        >
                            {item.text}
                        </Link>
                    ))}
                </Box>
                <Typography
                    variant='h4'
                    component='p'
                    color='common.white'
                    fontFamily="'Mochiy Pop P One', sans-serif"
                >
                    Djole&Kvels
                </Typography>
                <Typography
                    color='common.white'
                    fontSize='2rem'
                    fontWeight='bold'
                >
                    &copy;
                    {currentYear}
                </Typography>
            </Box>
        </footer>
    );
};

export default Footer;
