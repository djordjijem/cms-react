import React from 'react';

import styles from '../../../styles/layout/navbar.module.scss';
import {
    SwipeableDrawer,
    Divider,
    IconButton,
    SwipeableDrawerProps,
} from '@mui/material';
import { MdOutlineClose } from 'react-icons/md';

interface NavbarDrawerProps extends SwipeableDrawerProps {
    list: JSX.Element;
}

const NavbarDrawer: React.FC<NavbarDrawerProps> = ({
    open,
    onOpen,
    onClose,
    list,
}) => {
    return (
        <SwipeableDrawer
            open={open}
            onOpen={onOpen}
            onClose={onClose}
            anchor='right'
            classes={{
                paper: styles.navbar__drawer,
            }}
        >
            <IconButton
                className={styles['navbar__options-drawer-close-btn']}
                onClick={onClose}
            >
                <MdOutlineClose color='white' />
            </IconButton>

            <Divider className={styles['navbar__drawer-divider']} />

            {list}
        </SwipeableDrawer>
    );
};

export default NavbarDrawer;
