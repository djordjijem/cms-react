import React, { useState } from 'react';

import Link from 'next/link';

import NavbarDrawer from '../navbar-drawer/NavbarDrawer';

import { APPBAR_UP_ITEMS } from '../../../static';

import styles from '../../../styles/layout/navbar.module.scss';
import {
    Toolbar,
    IconButton,
    Link as MuiLink,
    Hidden,
    List,
    ListItem,
} from '@mui/material';
import { FaTools } from 'react-icons/fa';

import { RootState } from '../../../redux/store';
import { useSelector, useDispatch } from 'react-redux';
import { adminSlice } from '../../../redux/slices/adminSlice';

const NavbarUpSection: React.FC = () => {
    const isAdminActive = useSelector(
        (state: RootState) => state.isAdminActive.isAdminActive,
    );

    const title = useSelector((state: RootState) => state.title.title);

    const [openOptions, setOpenOptions] = useState(false);

    const dispatch = useDispatch();

    const handleAdminClick = () => {
        if (!title) return;

        dispatch(adminSlice.actions.setIsAdminActive());
    };

    const handleOpenOptions = () => setOpenOptions(true);

    const handleCloseOptions = () => setOpenOptions(false);

    const DrawerList = (
        <List className={styles['navbar__drawer-list']}>
            {APPBAR_UP_ITEMS.map((item) => (
                <ListItem
                    className={styles['navbar__drawer-item']}
                    key={item.id}
                >
                    <IconButton
                        className={styles.navbar__option}
                        onClick={handleAdminClick}
                        color={isAdminActive ? 'error' : 'success'}
                    >
                        {item.icon}
                    </IconButton>
                </ListItem>
            ))}
        </List>
    );

    return (
        <>
            <Toolbar className={styles['navbar__toolbar-up']}>
                <Link href='/'>
                    <MuiLink
                        className={styles.navbar__logo}
                        variant='h3'
                        flexGrow={1}
                        pt={2}
                        underline='none'
                        fontFamily="'Mochiy Pop P One', sans-serif"
                        fontSize='2.5rem'
                        color='common.white'
                    >
                        Djole&Kvels
                    </MuiLink>
                </Link>

                <Hidden smDown>
                    {APPBAR_UP_ITEMS.map((item) => (
                        <IconButton
                            className={styles.navbar__option}
                            onClick={handleAdminClick}
                            key={item.id}
                            size='large'
                            color={isAdminActive ? 'error' : 'success'}
                        >
                            {item.icon}
                        </IconButton>
                    ))}
                </Hidden>

                <Hidden smUp>
                    <IconButton
                        className={styles['navbar__options-btn']}
                        onClick={handleOpenOptions}
                        color='inherit'
                    >
                        <FaTools />
                    </IconButton>
                </Hidden>
            </Toolbar>

            <NavbarDrawer
                open={openOptions}
                onOpen={handleOpenOptions}
                onClose={handleCloseOptions}
                list={DrawerList}
            />
        </>
    );
};

export default NavbarUpSection;
