import React, { useState } from 'react';

import CustomButton from '../../button/CustomButton';
import NavbarDrawer from '../navbar-drawer/NavbarDrawer';

import { APPBAR_DOWN_ITEMS } from '../../../static';

import styles from '../../../styles/layout/navbar.module.scss';
import { FaBars } from 'react-icons/fa';
import {
    Toolbar,
    Box,
    IconButton,
    Hidden,
    List,
    ListItem,
} from '@mui/material';

const NavbarDownSection: React.FC = () => {
    const [openLinks, setOpenLinks] = useState(false);

    const handleOpenLinks = () => setOpenLinks(true);

    const handleCloseLinks = () => setOpenLinks(false);

    const DrawerList = (
        <List className={styles['navbar__drawer-list']}>
            {APPBAR_DOWN_ITEMS.map((item) => (
                <ListItem
                    className={styles['navbar__drawer-item']}
                    key={item.id}
                >
                    <CustomButton
                        className={styles.navbar__link}
                        text={item.text}
                    />
                </ListItem>
            ))}
        </List>
    );

    return (
        <>
            <Toolbar className={styles['navbar__toolbar-down']}>
                <Hidden smDown>
                    {APPBAR_DOWN_ITEMS.map((item) => (
                        <CustomButton
                            className={styles.navbar__link}
                            key={item.id}
                            text={item.text}
                        />
                    ))}
                </Hidden>

                <Hidden smUp>
                    <Box width='100%' display='flex' justifyContent='flex-end'>
                        <IconButton
                            className={styles['navbar__links-btn']}
                            onClick={handleOpenLinks}
                            color='inherit'
                        >
                            <FaBars />
                        </IconButton>
                    </Box>
                </Hidden>
            </Toolbar>

            <NavbarDrawer
                open={openLinks}
                onOpen={handleOpenLinks}
                onClose={handleCloseLinks}
                list={DrawerList}
            />
        </>
    );
};

export default NavbarDownSection;
