import React from 'react';

import { AppBar, Toolbar, Box } from '@mui/material';

import NavbarUpSection from './navbar-sections/NavbarUpSection';
import NavbarDownSection from './navbar-sections/NavbarDownSection';

const Navbar: React.FC = () => {
    return (
        <Box flexGrow={1} mb={4}>
            <Toolbar />
            <Toolbar />
            <AppBar position='fixed'>
                <NavbarUpSection />

                <NavbarDownSection />
            </AppBar>
        </Box>
    );
};

export default Navbar;
