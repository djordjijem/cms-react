export const disableSubmitOnEnterKey = (e: {
    key: string;
    preventDefault: () => void;
}) => {
    e.key === 'Enter' && e.preventDefault();
}; 