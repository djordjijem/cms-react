import {
    AbstractRepository,
    EntityRepository,
    SelectQueryBuilder,
} from 'typeorm';
import { Post } from './post.entity';

@EntityRepository(Post)
export class PostRepository extends AbstractRepository<Post> {
    public async findOne(id: string): Promise<Post> {
        return this.manager.findOne(Post, {
            where: {
                id,
            },
        });
    }

    public async findAllForUser(userId: string): Promise<Post[]> {
        return this.q(userId).getMany();
    }

    public async save(entity: Post): Promise<Post> {
        return this.manager.save(entity);
    }

    public async delete(entity: Post): Promise<Post> {
        return this.manager.remove(entity);
    }

    private q(userId: string): SelectQueryBuilder<Post> {
        return this.createQueryBuilder('Post').where(
            '"Post"."userId" = :userId',
            { userId },
        );
    }
}
