import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

export interface PostContent {
    id: number;
    heading: string;
    text: string;
    imgSrc: string;
    subtopics: Array<{ id: number; heading: string; text: string }>;
}

@Entity('Post')
export class Post {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public deletedAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

    @Column('jsonb')
    public content: PostContent;

    constructor(data?: Partial<Post>) {
        Object.assign(this, data);
    }
}
