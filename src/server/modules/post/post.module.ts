import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostRepository } from './post.repository';

@Module({
    imports: [TypeOrmModule.forFeature([PostRepository])],
    controllers: [],
    providers: [],
    exports: [TypeOrmModule.forFeature([PostRepository])],
})
export class PostModule {}
