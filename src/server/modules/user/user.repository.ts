import {
    AbstractRepository,
    EntityRepository,
    SelectQueryBuilder,
} from 'typeorm';
import { User } from './user.entity';

@EntityRepository(User)
export class UserRepository extends AbstractRepository<User> {
    public async findOne(id: string): Promise<User> {
        return this.manager.findOne(User, {
            where: {
                id,
            },
        });
    }

    public async findOneByEmail(email: string): Promise<User> {
        return this.q().andWhere('"email" = :email', { email }).getOne();
    }

    public async save(entity: User): Promise<User> {
        return this.manager.save(entity);
    }

    public async delete(entity: User): Promise<User> {
        return this.manager.remove(entity);
    }

    private q(): SelectQueryBuilder<User> {
        return this.createQueryBuilder('User');
    }
}
