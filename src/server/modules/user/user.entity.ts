import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity('User')
export class User {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'varchar', length: 255, unique: true })
    public email: string;

    @Column({ type: 'varchar', length: 255, nullable: true })
    public passwordHash: string;

    @Column('text', { array: true, nullable: true })
    public passwordHistory: string[] = [];

    @Column({ type: 'timestamptz' })
    public passwordChangedAt: Date;

    @Column('integer')
    public invalidLoginCount: number;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

    constructor(data?: Partial<User>) {
        Object.assign(this, data);
    }
}
