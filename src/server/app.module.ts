import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

import { ViewModule } from './modules/view/view.module';
import { AppConfigService } from './core/config/appConfig.service';
import { NestConfigModuleRootConfigurationDynamicModule } from './core/config/nestConfigModuleRootConfigurationDynamicModule';
import { CoreModule } from './core/core.module';
import { PostRepository } from './modules/post/post.repository';
import { UserRepository } from './modules/user/user.repository';
import { PostModule } from './modules/post/post.module';
import { UserModule } from './modules/user/user.module';

@Module({
    imports: [
        ScheduleModule.forRoot(),
        ViewModule,
        NestConfigModuleRootConfigurationDynamicModule,
        TypeOrmModule.forRootAsync({
            useFactory: async (appConfig: AppConfigService) => ({
                type: 'postgres',
                ...appConfig.db,
                entities: [__dirname + '/modules/**/*.entity{.ts,.js}'],
            }),
            imports: [CoreModule],
            inject: [AppConfigService],
        }),
        TypeOrmModule.forFeature([UserRepository, PostRepository]),
        PostModule,
        UserModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
