import { ConfigModule } from '@nestjs/config';

export const NestConfigModuleRootConfigurationDynamicModule =
    ConfigModule.forRoot({
        envFilePath: [
            `.env.${process.env.NODE_ENV}.local`,
            `.env.local`,
            `.env.${process.env.NODE_ENV}`,
            `.env`,
        ],
    });
