import { Module } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestConfigModuleRootConfigurationDynamicModule } from './nestConfigModuleRootConfigurationDynamicModule';
import { AppConfigService } from './appConfig.service';
import { AppConfigModule } from './appConfig.module';

export class ConfigUtils {
    static async getStandaloneConfigServiceNestApp(): Promise<AppConfigService> {
        @Module({
            imports: [
                NestConfigModuleRootConfigurationDynamicModule,
                AppConfigModule,
            ],
        })
        class DummyModule {}

        const app = await NestFactory.create(DummyModule, {
            logger: ['warn', 'error'],
        });

        const appConfigService = app.get(AppConfigService);
        return appConfigService;
    }
}
