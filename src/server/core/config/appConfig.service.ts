import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PostgresConnectionCredentialsOptions } from 'typeorm/driver/postgres/PostgresConnectionCredentialsOptions';

@Injectable()
export class AppConfigService {
    constructor(private configService: ConfigService) {}

    get env() {
        return this.configService.get('NODE_ENV');
    }
    get envPrefix() {
        return this.configService.get('NODE_ENV').split('-')[0];
    }
    get app() {
        return {
            host: this.configService.get('HOST'),
            port: parseInt(this.configService.get('PORT'), 10) || 3000,
        };
    }

    get db(): PostgresConnectionCredentialsOptions {
        return {
            host: this.configService.get('POSTGRES_HOST'),
            port: Number(this.configService.get('POSTGRES_PORT')),
            username: this.configService.get('POSTGRES_USER'),
            password: this.configService.get('POSTGRES_PASSWORD'),
            database: this.configService.get('POSTGRES_DB'),
            ssl: this.configService.get('POSTGRES_SSL') === 'true',
        };
    }
}
