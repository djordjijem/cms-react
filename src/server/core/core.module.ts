import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from '../modules/user/user.repository';
import { AppConfigModule } from './config/appConfig.module';
import { PostRepository } from '../modules/post/post.repository';

//import { InitAuthenticationContextMiddleware } from './auth/initAuthenticationContext.middleware';

@Module({
    imports: [
        TypeOrmModule.forFeature([UserRepository, PostRepository]),
        AppConfigModule,
    ],

    exports: [AppConfigModule],
})
export class CoreModule {
    // configure(consumer: MiddlewareConsumer): void {
    //     consumer.apply(InitAuthenticationContextMiddleware).forRoutes('*');
    // }
}
