import { UserRoleEnum } from '../enums/userRole.enum';

const usersData = [
    {
        email: 'cmsreact_admin@yopmail.com',
        roleId: UserRoleEnum.SUPERADMIN,
        firstName: 'Djordjije',
        lastName: 'Matic',
        userId: process.env.CMSREACT_SYSTEM_USER_ID,
    },
];

export const userData: any[] = [];

seedUsers(usersData);

function seedUsers(seedingUsers: any[]) {
    seedingUsers.forEach((seedData) => {
        userData.push({
            id: seedData.userId,
            email: seedData.email,
            passwordHash: 'NOPASSWORD',
            roleId: seedData.roleId,
        });
    });
}
