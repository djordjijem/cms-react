import { NestFactory } from '@nestjs/core';
import { Connection } from 'typeorm';

import { AppModule } from '../app.module';
import { blogPostsData } from './blogPosts.seed';
import { PostRepository } from '../modules/post/post.repository';
import { Post } from '../modules/post/post.entity';
import { UserRepository } from '../modules/user/user.repository';
import { userData } from './users.seed';
import { User } from '../modules/user/user.entity';

async function seed() {
    console.log(`Seed Base Starting #${process.pid}`);
    const app = await NestFactory.create(AppModule, {
        logger: false,
    });
    app.useLogger(null);
    const db = app.get<Connection>(Connection);

    console.log('\x1b[33m%s\x1b[0m', 'Seeding Users');
    const userRepo = db.getCustomRepository(UserRepository);
    await Promise.all(
        userData.map(async (data) => {
            let user = await userRepo.findOne(data.id);
            if (user === undefined) {
                user = new User();
            }

            Object.assign(user, data);
            await userRepo.save(user);
        }),
    );

    console.log('\x1b[33m%s\x1b[0m', 'Seeding Blog posts');
    await Promise.all(
        blogPostsData.map(async (data) => {
            let obj = await db
                .getCustomRepository(PostRepository)
                .findOne(data.id.toString());
            if (obj === undefined) {
                obj = new Post();
            }
            Object.assign(obj, data);
            return db.getCustomRepository(PostRepository).save(obj);
        }),
    );
}

seed().then(
    () => process.exit(0),
    (err) => {
        console.error(err.stack);
        process.exit(1);
    },
);
