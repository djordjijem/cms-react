export enum UserRoleEnum {
    SUPERADMIN = 'SUPERADMIN',
    ADMIN = 'ADMIN',
    GUEST = 'GUEST',
}
