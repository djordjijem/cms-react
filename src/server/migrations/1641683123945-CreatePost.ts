import { MigrationInterface, QueryRunner } from 'typeorm';
import { addFK, addPK } from './MigrationHelper';

export class CreatePost1641683123945 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "Post" (
          "id" uuid NOT NULL DEFAULT gen_random_uuid(),
          "userId" uuid NOT NULL,
          "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
          "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
          "deletionDate" TIMESTAMP DEFAULT NULL,
          "content" jsonb NOT NULL DEFAULT '{}'::jsonb,
          ${addPK('Post', ['id', 'userId'])},
          ${addFK('Post', ['userId'], 'User', ['id'])}
    )`,
            undefined,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        //nothing here
    }
}
