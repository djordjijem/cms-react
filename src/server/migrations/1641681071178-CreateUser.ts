import { MigrationInterface, QueryRunner } from 'typeorm';
import { addPK, addUQ } from './MigrationHelper';

export class CreateUser1641681071178 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "User" (
      "id" uuid NOT NULL DEFAULT gen_random_uuid(),
      "email" character varying(255) NOT NULL,
      "firstName" character varying(255),
      "lastName" character varying(255),
      "passwordHash" character varying(255) NOT NULL,
      "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
      "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
      "deletionDate" TIMESTAMP DEFAULT NULL,
      ${addPK('User', ['id'])},
      ${addUQ('User', ['email'])}
    )`,
            undefined,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        //nothing here
    }
}
